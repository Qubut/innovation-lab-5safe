# Innovation Lab Project


## Dataset

The [dataset](./il_5safe/data/traffic/input/)
consists of 3980 images from two videos for an experiment to be used for detecting bikers and pedastrians.

## Faster-RCNN model

Most of the model's code is based on PyTorch's Faster-RCNN implementation. Metrics can be computed based on
the [PASCAL VOC](http://host.robots.ox.ac.uk/pascal/VOC/) (**V**isual **O**bject **C**lasses) evaluator in
the [metrics section](pytorch_faster_rcnn_tutorial/metrics).

## Anchor Sizes/Aspect Ratios

Anchor sizes/aspect ratios are really important for training a Faster-RCNN model (but also similar models like SSD,
YOLO). These "default" boxes are compared to those outputted by the network, therefore choosing adequate sizes/ratios
can be critical for the success of a project. The PyTorch implementation of the AnchorGenerator (and also the helper
classes here) generally expect the following format:

- anchor_size: `Tuple[Tuple[int, ...], ...]`
- aspect_ratios: `Tuple[Tuple[float, ...]]`

### Without FPN

The ResNet backbone without the FPN always returns a single feature map that is used to create anchor boxes. Because of
that we must create a `Tuple` that contains a single `Tuple`: e.g. `((32, 64, 128, 256, 512),)` or `(((32, 64),)`

### With FPN

With FPN we can use 4 feature maps (output from a ResNet + FPN) and map our anchor sizes with the feature maps. Because
of that we must create a `Tuple` that contains exactly **4** `Tuples`

### Files

   - convert_coco.py: This file contains the script for processing COCO data from a JSON file. It filters bounding boxes with negative coordinates, creates target dictionaries, and saves them as JSON files corresponding to the image frames. 
   - training_script.py: This file contains the main script for training the Faster-RCNN model.
   - .env: has the API-Key in the `NEPTUNE` variable for neptune logging
## Prerequisites

Before running the scripts, make sure you have the following dependencies installed in the `requirements.txt`
or you can run `poetry install` to install them using `pypoetry`

## Installation

After cloning the repository, follow these steps to install the dependencies in a new environment:

1. Install Git LFS:
    1. Download and install Git LFS from the official website: [https://git-lfs.github.com/](https://git-lfs.github.com/)
    2. Set up Git LFS by running the following command in the cloned repository:
       ```
       git lfs install
       ```

2. Set up Git LFS to retrieve all the files:
    1. Run the following command to fetch all the large files tracked by Git LFS:
       ```
       git lfs fetch
       ```
    2. Run the following command to pull the files into your local repository:
       ```
       git lfs pull
       ```

3. Set up & activate a new environment with an environment manager (recommended):
    1. [poetry](https://python-poetry.org/):
        1. `poetry env use python3.10`
        2. `source .venv/bin/activate`
    2. [venv](https://docs.python.org/3/library/venv.html):
        1. `python3 -m venv .venv`
        2. `source .venv/bin/activate`
    3. [conda](https://docs.conda.io/en/latest/miniconda.html):
        1. `conda create --name faster-rcnn-tutorial -y`
        2. `conda activate faster-rcnn-tutorial`
        3. `conda install python=3.10 -y`

2. Install the libraries with **pip** or **poetry**:
    1. [poetry](https://python-poetry.org/):
        1. `poetry install` (poetry.lock)

## Usage

### Training:

To train Faster-RCNN: `poetry run python il_5safe/training_script.py`
the trained model will be outputed to  ``il_5safe/out``
#### COCO Data Conversion:
 
1. export the variable `IS_TESTING_DATA=1` if want to convert `il_5safe/data/traffic/testing.json`, otherwise `il_5safe/data/traffic/training.json` will be processed, run  `unset IS_TESTING_DATA` to undo the exporting 
2. run:  `poetry run python il_5safe/convert_coco.py`
The labels will be then be found in `il_5safe/data/traffic/target`