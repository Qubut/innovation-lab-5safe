import logging
import os
import pathlib
import sys
from dataclasses import asdict
from typing import Dict, List
from pydantic import BaseSettings, Field
from pytorch_lightning import Trainer, seed_everything
from pytorch_lightning.callbacks import (
    EarlyStopping,
    LearningRateMonitor,
    ModelCheckpoint,
)
from torch.utils.data import DataLoader
from torchvision.models.detection.faster_rcnn import FasterRCNN
from lightning.pytorch.loggers import NeptuneLogger

from utils.datasets import ObjectDetectionDataSet
from models.faster_RCNN import (
    FasterRCNNLightning,
    get_faster_rcnn_resnet,
)
from utils.utils import (
    collate_double,
    get_filenames_of_path,
    log_model_neptune,
)
from models.parameters import Parameters

# Change the working directory to the directory containing this script
os.chdir(os.path.dirname(os.path.abspath(__file__)))

logger: logging.Logger = logging.getLogger(__name__)

# logging
logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s - %(levelname)s - %(filename)s:%(lineno)d:%(funcName)s - %(message)s",
    datefmt="%Y-%m-%d %H:%M:%S",
    handlers=[logging.StreamHandler(sys.stdout)],
)

# root directory (project directory)
ROOT_PATH: pathlib.Path = pathlib.Path(__file__).parent.absolute()


class NeptuneSettings(BaseSettings):
    """
    Reads the variables from the environment.
    Errors will be raised if the required variables are not set.
    """

    api_key: str = Field(default=..., env="NEPTUNE")
    OWNER: str = "iotinnovation832"  # set your name here, e.g. johndoe22
    PROJECT: str = "Faster-RCNN"  # set your project name here, e.g. Heads
    EXPERIMENT: str = (
        "Pedatrian_Biker"  # set your experiment name here, e.g. heads
    )

    class Config:
        # this tells pydantic to read the variables from the .env file
        env_file = ".env"


def train():
    # environment variables (pydantic BaseSettings class)
    neptune_settings: NeptuneSettings = NeptuneSettings()

    # parameters (dataclass)
    parameters: Parameters = Parameters()

    # data path relative to this file (pathlib)
    data_path: pathlib.Path = ROOT_PATH / "data" / "traffic"

    # input and target files
    inputs: List[pathlib.Path] = get_filenames_of_path(data_path / "input")
    targets: List[pathlib.Path] = get_filenames_of_path(data_path / "target")

    # sort inputs and targets
    inputs.sort()
    targets.sort()
    # mapping
    mapping: Dict[str, int] = {"Pedestrian": 1, "Biker": 2}

    # random seed (function that sets seed for pseudo-random number generators in: pytorch, numpy, python.random)
    seed_everything(parameters.SEED)

    # training validation test split (manually)
    inputs_train, inputs_valid, inputs_test = (
        inputs[:2000],
        inputs[2000:2700],
        inputs[2700:],
    )
    targets_train, targets_valid, targets_test = (
        targets[:2000],
        targets[2000:2700],
        targets[2700:],
    )

    # dataset training
    dataset_train: ObjectDetectionDataSet = ObjectDetectionDataSet(
        inputs=inputs_train,
        targets=targets_train,
        use_cache=parameters.CACHE,
        convert_to_format=None,
        mapping=mapping,
    )

    # dataset validation
    dataset_valid: ObjectDetectionDataSet = ObjectDetectionDataSet(
        inputs=inputs_valid,
        targets=targets_valid,
        use_cache=parameters.CACHE,
        convert_to_format=None,
        mapping=mapping,
    )

    # dataset test
    dataset_test: ObjectDetectionDataSet = ObjectDetectionDataSet(
        inputs=inputs_test,
        targets=targets_test,
        use_cache=parameters.CACHE,
        convert_to_format=None,
        mapping=mapping,
    )

    # dataloader training
    dataloader_train: DataLoader = DataLoader(
        dataset=dataset_train,
        batch_size=parameters.BATCH_SIZE,
        shuffle=True,
        num_workers=4,
        collate_fn=collate_double,
    )

    # dataloader validation
    dataloader_valid: DataLoader = DataLoader(
        dataset=dataset_valid,
        batch_size=1,
        shuffle=False,
        num_workers=5,
        collate_fn=collate_double,
    )

    # dataloader test
    dataloader_test: DataLoader = DataLoader(
        dataset=dataset_test,
        batch_size=1,
        shuffle=False,
        num_workers=4,
        collate_fn=collate_double,
    )

    # neptune logger (neptune-client)
    neptune_logger: NeptuneLogger = NeptuneLogger(
        api_key=neptune_settings.api_key,
        project=f"{neptune_settings.OWNER}/{neptune_settings.PROJECT}",  # use your neptune name here
        name=neptune_settings.PROJECT,
        log_model_checkpoints=False,
    )

    # # log hyperparameters
    neptune_logger.log_hyperparams(asdict(parameters))

    # model init
    model: FasterRCNN = get_faster_rcnn_resnet(
        num_classes=parameters.CLASSES,
        backbone_name=parameters.BACKBONE,
        anchor_size=parameters.ANCHOR_SIZE,
        aspect_ratios=parameters.ASPECT_RATIOS,
        fpn=parameters.FPN,
        min_size=parameters.MIN_SIZE,
        max_size=parameters.MAX_SIZE,
    )

    # lightning model
    model: FasterRCNNLightning = FasterRCNNLightning(
        model=model, lr=parameters.LR, iou_threshold=parameters.IOU_THRESHOLD
    )

    # callbacks
    checkpoint_callback: ModelCheckpoint = ModelCheckpoint(
        monitor="Validation_mAP", mode="max"
    )
    learning_rate_callback: LearningRateMonitor = LearningRateMonitor(
        logging_interval="step", log_momentum=True
    )
    early_stopping_callback: EarlyStopping = EarlyStopping(
        monitor="Validation_mAP", patience=parameters.PATIENCE, mode="max"
    )

    # trainer init
    trainer: Trainer = Trainer(
        accelerator=parameters.ACCELERATOR,
        logger=neptune_logger,
        callbacks=[
            checkpoint_callback,
            learning_rate_callback,
            early_stopping_callback,
        ],
        default_root_dir=parameters.SAVE_DIR,  # where checkpoints are saved to
        log_every_n_steps=1,  # increase to reduce the amount of log flushes (lowers the overhead)
        num_sanity_val_steps=0,  # set to 0 to skip sanity check
        max_epochs=parameters.MAXEPOCHS,
        fast_dev_run=parameters.FAST_DEV_RUN,  # set to True to test the pipeline with one batch and without validation, testing and logging
        enable_checkpointing=True,
    )

    # start training
    trainer.fit(
        model=model,
        train_dataloaders=dataloader_train,
        val_dataloaders=dataloader_valid,
    )

    if not parameters.FAST_DEV_RUN:
        # start testing
        trainer.test(ckpt_path="best", dataloaders=dataloader_test)

        # log model

    if parameters.LOG_MODEL:
        checkpoint_path = pathlib.Path(checkpoint_callback.best_model_path)
        save_directory = parameters.SAVE_DIR
        model_filename = "best_model.pt"

        index = 0
        new_model_filename = model_filename

        while os.path.exists(os.path.join(save_directory, new_model_filename)):
            index += 1
            new_model_filename = f"best_model_{index}.pt"

        log_model_neptune(
            checkpoint_path=checkpoint_path,
            save_directory=save_directory,
            name=new_model_filename,
            neptune_logger=neptune_logger,
        )
    # stop logger
    neptune_logger.experiment.stop()
    logger.info("Training finished")


if __name__ == "__main__":
    train()
