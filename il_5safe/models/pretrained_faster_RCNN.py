import torch
from torchvision.models.detection import fasterrcnn_resnet50_fpn


class PretrainedModel:
    def __init__(self, state_dict=None):
        self.device = torch.device(
            "cuda" if torch.cuda.is_available() else "cpu"
        )
        self.model = self._load_model(state_dict)

    def _load_model(self, state_dict=None):
        # Set the device to the available GPUs
        model = fasterrcnn_resnet50_fpn(pretrained=True)
        # model = torch.nn.DataParallel(model)
        if state_dict:
            model.load_state_dict(state_dict, strict=False)
        model.to(self.device)
        model.eval()
        return model

    def predict(self, image_tensor):
        with torch.no_grad():
            predictions = self.model(image_tensor)
        return predictions
