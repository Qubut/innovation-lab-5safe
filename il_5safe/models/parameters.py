import pathlib
from dataclasses import dataclass, field
from typing import Optional, Tuple, List
from utils.backbone_resnet import ResNetBackbones

# root directory (project directory)
ROOT_PATH: pathlib.Path = pathlib.Path(__file__).parent.absolute()


@dataclass
class Parameters:
    """
    Dataclass for the parameters.
    """

    BATCH_SIZE: int = 32
    CACHE: bool = False
    SAVE_DIR: pathlib.Path = (
        ROOT_PATH / "out"
    )  # checkpoints will be saved to cwd (current working directory) if None
    LOG_MODEL: bool = (
        True  # whether to log the model to neptune after training
    )
    ACCELERATOR: Optional[str] = "auto"  # set to "gpu" if you want to use GPU
    LR: float = 0.001
    PRECISION: int = 32
    CLASSES: int = 3
    SEED: int = 40
    MAXEPOCHS: int = 500
    PATIENCE: int = 50
    BACKBONE: ResNetBackbones = ResNetBackbones.RESNET34
    FPN: bool = False
    if FPN:
        ANCHOR_SIZE: Tuple[Tuple[int, ...], ...] = (
            (32, 64, 128, 256, 512),
        ) * 4  # Repeat the tuple 4 times for 4 feature maps
        ASPECT_RATIOS: Tuple[Tuple[float, ...], ...] = (
            (0.5, 1.0, 2.0, 1.0, 1.0),
        ) * 4
    else:
        ANCHOR_SIZE: Tuple[Tuple[int, ...], ...] = ((32, 64, 128, 256, 512),)
        ASPECT_RATIOS: Tuple[Tuple[float, ...]] = ((0.5, 1.0, 2.0),)
    MIN_SIZE: int = 1080
    MAX_SIZE: int = 1920
    IMG_MEAN: List = field(default_factory=lambda: [0.485, 0.456, 0.406])
    IMG_STD: List = field(default_factory=lambda: [0.229, 0.224, 0.225])
    IOU_THRESHOLD: float = 0.5
    FAST_DEV_RUN: bool = False

    def __post_init__(self):
        if self.SAVE_DIR is None:
            self.SAVE_DIR: str = str(pathlib.Path.cwd())
