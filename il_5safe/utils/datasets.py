import logging
import pathlib
from multiprocessing import Pool
from typing import List
import torch
from skimage.color import rgba2rgb
from skimage.io import imread
from torch.utils.data import Dataset
from torchvision.ops import box_convert
import torchvision.transforms as transforms
from utils.transformations import map_class_to_int
from utils.utils import read_json

logger = logging.getLogger(__name__)


class ObjectDetectionDataSet(Dataset):
    def __init__(
        self,
        inputs: List[pathlib.Path],
        targets: List[pathlib.Path],
        transform=None,
        use_cache=False,
        convert_to_format=None,
        mapping=None,
    ):
        self.inputs = inputs
        self.targets = targets
        self.transform = transform
        self.use_cache = use_cache
        self.convert_to_format = convert_to_format
        self.mapping = mapping

        if self.use_cache:
            with Pool() as pool:
                self.cached_data = pool.starmap(
                    self.read_images, zip(inputs, targets)
                )

    def __len__(self):
        return len(self.inputs)

    def __getitem__(self, index: int):
        if self.use_cache:
            x, y = self.cached_data[index]
        else:
            input_ID = self.inputs[index]
            target_ID = self.targets[index]
            x, y = self.read_images(input_ID, target_ID)

        if x.shape[-1] == 4:
            x = rgba2rgb(x)

        boxes = torch.tensor(y["boxes"], dtype=torch.float32)

        if "scores" in y.keys():
            scores = torch.tensor(y["scores"], dtype=torch.float32)

        if self.mapping:
            labels = map_class_to_int(y["labels"], mapping=self.mapping)
        else:
            labels = y["labels"]

        labels = torch.from_numpy(labels).to(torch.int64)

        if self.convert_to_format == "xyxy":
            boxes = box_convert(boxes, in_fmt="xywh", out_fmt="xyxy")
        elif self.convert_to_format == "xywh":
            boxes = box_convert(boxes, in_fmt="xyxy", out_fmt="xywh")

        target = {"boxes": boxes, "labels": labels}

        if "scores" in y.keys():
            target["scores"] = scores

        x = transforms.ToTensor()(x)
        x = transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])(
            x
        )

        target = {key: value.numpy() for key, value in target.items()}
        x = x.type(torch.float32)
        target = {
            key: torch.from_numpy(value).type(torch.int64)
            for key, value in target.items()
        }

        return {
            "x": x,
            "y": target,
            "x_name": self.inputs[index].name,
            "y_name": self.targets[index].name,
        }

    @staticmethod
    def read_images(inp, tar):
        return imread(inp), read_json(tar)


class ObjectDetectionDatasetSingle(Dataset):
    def __init__(self, inputs, transform=None, use_cache=False):
        self.inputs = inputs
        self.transform = transform
        self.use_cache = use_cache

        if self.use_cache:
            with Pool() as pool:
                self.cached_data = pool.starmap(
                    func=self.read_images, iterable=inputs
                )

    def __len__(self):
        return len(self.inputs)

    def __getitem__(self, index):
        if self.use_cache:
            x = self.cached_data[index]
        else:
            input_id = self.inputs[index]
            x = self.read_images(input_id)

        if x.shape[-1] == 4:
            x = rgba2rgb(x)

        x = self.transform(x) if self.transform is not None else x

        x = transforms.ToTensor()(x)
        x = transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])(
            x
        )

        x = x.type(torch.float32)

        return {"x": x, "x_name": self.inputs[index].name}

    @staticmethod
    def read_images(inp):
        return imread(inp)
