import json
import os
from typing import Any, List, Dict
import pathlib


def load_annotations(json_file: pathlib.Path | str) -> Dict[str, Any]:
    """
    Load annotations from a JSON file.

    Args:
        json_file (Path or str): Path to the JSON file.

    Returns:
        dict: Loaded JSON data.

    Raises:
        FileNotFoundError: If the JSON file does not exist.
        json.JSONDecodeError: If there is an error decoding the JSON file.
    """
    with open(json_file, "r") as f:
        data = json.load(f)
    return data


def get_image_id(image_path: pathlib.Path | str) -> int:
    """
    Get the image ID from the image path.

    Args:
        image_path (Path or str): Path to the image file.

    Returns:
        int: Image ID.

    Raises:
        ValueError: If the image path is invalid or does not contain the image ID.
    """
    image_file = os.path.basename(image_path)
    image_id = int(image_file.split(".")[0].split("_")[1])
    return image_id


def get_targets(
    annotations: List[Dict[str, Any]], image_id: str
) -> List[Dict[str, Any]]:
    """
    Get the targets for a specific image ID from a list of annotations.

    Args:
        annotations (List[Dict[str, Any]]): List of annotation dictionaries.
        image_id (str): Image ID.

    Returns:
        List[Dict[str, Any]]: List of target dictionaries for the image ID.
    """
    try:
        annotation = [
            element
            for element in annotations
            if element["image_id"] == image_id
        ]
        return annotation
    except Exception:
        return []


def convert_predictions(
    predictions: List[Dict[str, List[float]]]
) -> List[Dict[str, List[float]]]:
    """
    Convert predictions to COCO format.

    Args:
        predictions (List[Dict[str, List[float]]]): List of prediction dictionaries.

    Returns:
        List[Dict[str, List[float]]]: List of COCO-formatted prediction dictionaries.
    """
    coco_predictions = []
    for prediction in predictions:
        boxes = prediction["boxes"].tolist()
        labels = prediction["labels"].tolist()
        scores = prediction["scores"].tolist()

        coco_prediction = {
            "bbox": boxes,
            "category_id": labels,
            "score": scores,
        }
        coco_predictions.append(coco_prediction)
    return coco_predictions
