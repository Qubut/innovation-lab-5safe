# Imports
import pathlib
from itertools import chain
import os
import json
from metrics.enumerators import MethodAveragePrecision
from metrics.pascal_voc_evaluator import (
    get_pascalvoc_metrics,
    plot_precision_recall_curve,
)
from utils.utils import (
    from_file_to_boundingbox,
    get_filenames_of_path,
)
from utils.logs_utils.logger import logger
import pandas as pd

# root directory
FOLDER_PATH = pathlib.Path(__file__).parent.absolute()
MODULE_PATH = FOLDER_PATH.parent.absolute()
DATA_PATH = MODULE_PATH / "il_5safe" / "data" / "traffic"


def main():
    directory = (
        DATA_PATH / "predictions"
    )  # Replace with the actual directory path

    for filename in os.listdir(directory):
        if filename.endswith(".json"):
            filepath = os.path.join(directory, filename)
            with open(filepath, "r") as file:
                data = json.load(file)

            labels = data["labels"]
            modified_labels = [
                "Pedestrian"
                if label == 1
                else "Biker"
                if label == 2
                else label.strip()
                for label in labels
            ]
            data["labels"] = modified_labels

            with open(filepath, "w") as file:
                json.dump(data, file)

    # input and target files
    predictions = get_filenames_of_path(directory)
    predictions.sort()
    gts = get_filenames_of_path(DATA_PATH / "target")
    gts.sort()
    # get the gt_boxes from disk
    gt_boxes = [
        from_file_to_boundingbox(file_name, groundtruth=True)
        for file_name in gts
    ]
    # reduce list
    gt_boxes = list(chain(*gt_boxes))

    pred_boxes = [
        from_file_to_boundingbox(file_name, groundtruth=False)
        for file_name in predictions
    ]
    pred_boxes = list(chain(*pred_boxes))

    results = get_pascalvoc_metrics(
        gt_boxes=gt_boxes,
        det_boxes=pred_boxes,
        iou_threshold=0.5,
        method=MethodAveragePrecision.EVERY_POINT_INTERPOLATION,
        generate_table=True,
    )
    per_class, m_ap, m_ar, f_score, m_ap_05 = (
        results["per_class"],
        results["m_ap"],
        results["m_ar"],
        results["f_score"],
        results["m_ap_05"],
    )
    results["method"] = "EVERY_POINT_INTERPOLATION"
    logger.info(
        f"""mAP: {m_ap}, mAP@0.5: {m_ap_05}, method: EVERY_POINT_INTERPOLATION,
                    mAR: {m_ar},
                    F-score: {f_score}
                """
    )
    plot_precision_recall_curve(per_class, save_path=DATA_PATH / "graphs")
    results_ = get_pascalvoc_metrics(
        gt_boxes=gt_boxes,
        det_boxes=pred_boxes,
        iou_threshold=0.5,
        method=MethodAveragePrecision.ELEVEN_POINT_INTERPOLATION,
        generate_table=True,
    )
    results_["method"] = "ELEVEN_POINT_INTERPOLATION"
    per_class, m_ap = results_["per_class"], results_["m_ap"]
    plot_precision_recall_curve(per_class, save_path=DATA_PATH / "graphs")
    logger.info(f"mAP: {m_ap}, method: ELEVEN_POINT_INTERPOLATION")
    unpack = lambda dic: {k: _ for k, _ in dic.items() if k != "per_class"}
    df = pd.DataFrame([unpack(results), unpack(results_)])
    logger.info(df.head())
    df.to_excel(DATA_PATH / "graphs" / "metrics.xlsx", index=False)


if __name__ == "__main__":
    main()
