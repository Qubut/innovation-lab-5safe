"""
This module processes COCO data from a JSON file by filtering bounding boxes with negative coordinates,
creating target dictionaries, and saving them as JSON files corresponding to the images frames.
"""
import json
import os
from utils.logs_utils.logger import logger

# Change the working directory to the directory containing this script
os.chdir(os.path.dirname(os.path.abspath(__file__)))


def get_frame_name(img) -> str:
    frame_name = img["file_name"].split("_")[1].split(".")[0][2:]
    return (
        str(int(frame_name) + 3000)
        if bool(os.environ.get("IS_TESTING_DATA"))
        else frame_name
    )


def convert_coordinates(coordinates):
    """Converts coordinates from [x, y, width, height] to [x_min, y_min, x_max, y_max]."""
    return [
        coordinates[0],
        coordinates[1],
        coordinates[0] + coordinates[2],
        coordinates[1] + coordinates[3],
    ]


def delete_img(imgs_dir, img_id):
    try:
        os.remove(f"{imgs_dir}/{img_id}.jpg")
    except Exception as e:
        logger.error(e)


def check_negativity(coordinates):
    """Checks if any coordinate value is negative."""
    return any(float(c) < 0 for c in coordinates)


def process_image(img, annotations, categories, imgs_dir, output_dir):
    """
    Processes an image by filtering bounding boxes with negative coordinates,
    creating the target dictionary, and saving it as a JSON file.

    Args:
        img (dict): The image dictionary.
        annotations (list): The list of annotation dictionaries.
        categories (list): The list of category dictionaries.
        imgs_dir (str): The directory path for the input images.
        output_dir (str): The directory path for the target JSON files.
    """
    is_test_data = bool(os.environ.get("IS_TESTING_DATA"))
    correct_index = lambda i: i + 3000 if is_test_data else i
    img_id = correct_index(int(img["id"]))
    logger.info(f"Processing bboxes of image: {img_id}")
    bboxes = [
        bbox_dict
        for bbox_dict in annotations
        if correct_index(int(bbox_dict["image_id"])) == img_id
        and not check_negativity(bbox_dict["bbox"])
    ]
    logger.info(f"Bounding boxes extracted: {len(bboxes)}")
    target = {
        "labels": [
            categories[bbox["category_id"] - 1]["name"] for bbox in bboxes
        ],
        "boxes": [convert_coordinates(bbox["bbox"]) for bbox in bboxes],
    }
    if not bboxes or len(target["labels"]) == 0 or len(target["boxes"]) == 0:
        logger.info(f"Image deleted: no frames in {get_frame_name(img)}")
        logger.info("removing the image")
        delete_img(imgs_dir, get_frame_name(img))
        return
    json_path = os.path.join(output_dir, f"{get_frame_name(img)}.json")
    with open(json_path, "w") as json_file:
        json.dump(target, json_file)
    logger.info(f"Finished processing {img_id}")


def process_traffic_data(json_file, output_dir, imgs_dir):
    """
    Processes the traffic data by loading the JSON file,
    iterating over the images, and calling the process_image function.

    Args:
        json_file (str): The path to the JSON file.
        output_dir (str): The directory path for the target JSON files.
        imgs_dir (str): The directory path for the input images.
    """
    with open(json_file, "r") as f:
        JSON_Data = json.load(f)

    images = JSON_Data["images"]
    categories = JSON_Data["categories"]
    annotations = JSON_Data["annotations"]

    counter = sum(
        1 for bbox_dic in annotations if not check_negativity(bbox_dic["bbox"])
    )
    for img in images:
        image_name = get_frame_name(img) + ".jpg"
        image_path = os.path.join(imgs_dir, image_name)

        if os.path.exists(image_path):
            process_image(img, annotations, categories, imgs_dir, output_dir)
        else:
            logger.warning(f"Image file not found: {image_name}")

    if counter > 0:
        logger.info(f"Processed bounding boxes: {counter}")


def main():
    if bool(os.environ.get("IS_TESTING_DATA")):
        json_file = "./data/traffic/testing.json"
        logger.info("processing COCO testing data")
    else:
        json_file = "./data/traffic/training.json"
        logger.info("processing COCO training data")

    output_dir = "./data/traffic/target"
    imgs_dir = "./data/traffic/input"
    process_traffic_data(json_file, output_dir, imgs_dir)


if __name__ == "__main__":
    main()
