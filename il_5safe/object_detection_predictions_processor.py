"""
Script for object detection using a pre-trained Faster R-CNN model and saving the predictions as JSON files.
"""

import os
import argparse
import pathlib
from PIL import Image
import torch
import torch.nn as nn
from torchvision.transforms import functional as F
from models import faster_RCNN
from models.faster_RCNN import FasterRCNNLightning, get_faster_rcnn_resnet
from utils.logs_utils.logger import logger
import json

from models.pretrained_faster_RCNN import PretrainedModel
from models.parameters import Parameters


class PredictionsProcessor:
    def __init__(self, model_path=None):
        """
        Initialize the predictions processor.

        Args:
            model_path (str): Path to the custom model. If None, use the pre-trained model.
        """
        self.model_path = model_path
        self.device = torch.device(
            "cuda" if torch.cuda.is_available() else "cpu"
        )
        self.model = self._load_model(model_path)

        # Output directory for predictions
        self.output_dir = (
            pathlib.Path(__file__).parent.absolute()
            / "data"
            / "traffic"
            / "predictions"
        )
        self.output_dir.mkdir(parents=True, exist_ok=True)

    def run(self):
        """
        Run the predictions processor.
        """
        input_dir = (
            pathlib.Path(__file__).parent.absolute()
            / "data"
            / "traffic"
            / "input"
        )
        for frame in os.scandir(input_dir):
            image = Image.open(frame.path).convert("RGB")

            # Preprocess the image
            image_tensor = F.to_tensor(image)
            image_tensor = image_tensor.unsqueeze(0).to(self.device)
            predictions = (
                self.model.forward(image_tensor)[0]
                if self.model_path
                else self.model.predict(image_tensor)[0]
            )
            # predictions = self.model.predict(image_tensor)[0]
            predictions["boxes"] = predictions["boxes"].tolist()
            predictions["labels"] = predictions["labels"].tolist()
            predictions["scores"] = predictions["scores"].tolist()

            file_name = frame.name.split(".")[0]

            prediction_file = self.output_dir / f"{file_name}.json"
            with open(prediction_file, "w") as f:
                json.dump(predictions, f)

            # logger.info(f"Predictions saved to: {prediction_file}")

    def _load_model(self, model_path):
        """
        Load the custom model using torch.

        Args:
            model_path (str): Path to the custom model.

        Returns:
            PretrainedModel: The loaded model.
        """
        if model_path is None:
            model = PretrainedModel()
        else:
            parameters = Parameters()
            state_dict = torch.load(model_path)
            # model init
            model = get_faster_rcnn_resnet(
                num_classes=parameters.CLASSES,
                backbone_name=parameters.BACKBONE,
                anchor_size=parameters.ANCHOR_SIZE,
                aspect_ratios=parameters.ASPECT_RATIOS,
                fpn=parameters.FPN,
                min_size=parameters.MIN_SIZE,
                max_size=parameters.MAX_SIZE,
            )

            # lightning model
            # model: FasterRCNNLightning = FasterRCNNLightning(
            #     model=model, lr=parameters.LR, iou_threshold=parameters.IOU_THRESHOLD
            # )
            # model = PretrainedModel(state_dict)

            model.load_state_dict(state_dict)
            if torch.cuda.device_count() > 1:
                model = nn.DataParallel(model)
            model = model.to(self.device)
            model.eval()
        return model


if __name__ == "__main__":
    # Parse the command-line arguments
    parser = argparse.ArgumentParser(description="Predictions Processor")
    parser.add_argument(
        "--model_path", type=str, default=None, help="Path to custom model"
    )
    args = parser.parse_args()

    # Create an instance of the predictions processor and run it
    processor = PredictionsProcessor(model_path=args.model_path)
    processor.run()
